package main

import (
	"fmt"
	"os"
	"github.com/Nik-U/pbc"
	"io/ioutil"
	"strconv"
	"strings"
	"log"
	"io"
	"crypto/sha256"
	"net/url"
	"context"
	"time"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"bitbucket.com/audit/other/func"
)

var pairing *pbc.Pairing
var g *pbc.Element
var u *pbc.Element
var pk *pbc.Element
var sk *pbc.Element

func timeTrack(start time.Time, name string) {
        elapsed := time.Since(start)
        fmt.Printf("function %s took %s\n", name, elapsed)
}

func err_message() {
	fmt.Printf("\nWelcome to Owner(client) program...\n")
	fmt.Printf("format: $ go build owner_n.go\n")
	fmt.Printf("\t$ ./owner_n PATH_TO_FILENAME\n\n")
	os.Exit(8)
}

func ContractVars() (*Audit, *bind.TransactOpts, *ethclient.Client) {
	gethPath, nodeKey := functions.GethPathAndKey()
    
    addrByte, err := ioutil.ReadFile("./tmp/contractInfo/contract-address")
	functions.CheckError(err)
    
    connection, err := ethclient.Dial(gethPath)
    functions.CheckError(err)
    fmt.Println("connection Returned: ", connection)
    
    instContract, err := NewAudit(common.HexToAddress(string(addrByte)), connection)
    functions.CheckError(err)
    fmt.Println("instContract Returned: ", instContract)
    
	auth, err := bind.NewTransactor(strings.NewReader(nodeKey), "asdfgh")
	functions.CheckError(err)

	return instContract, auth, connection
}

func main() {
	instContract, auth, connection := ContractVars()
	ctx := context.Background()

	paramsByte, GetGVal, GetUVal, err := instContract.GetParamsGU(nil)
	functions.CheckError(err)
	
	pairing, err = pbc.NewPairingFromString(string(paramsByte))
	functions.CheckError(err)

	g = pairing.NewG1().SetBytes(GetGVal)
	u = pairing.NewG1().SetBytes(GetUVal)
	sk = pairing.NewZr().Rand()
	pk = pairing.NewG1().PowZn(g, sk)
	
	functions.FileCrWr("./tmp/private/owner/skO", sk.Bytes())
	
	tx, err := instContract.SetPKO(auth, pk.Bytes())
	functions.CheckError(err)
	//fmt.Println("\nTxn hash is: ", tx.Hash())
	_, err = bind.WaitMined(ctx, connection, tx)
	functions.CheckError(err)
	GetPKOVal, err := instContract.GetPKO(nil)
	functions.CheckError(err)
	fmt.Println("\nGetPKO() Returned: ", GetPKOVal)

	msg := "\nconnecting to the server http://127.0.0.1:8003/...\n"
	msgtype := ""
	msgloc := "http://127.0.0.1:8003/"
	ret := functions.Connect(msgloc, msg, msgtype)
	fmt.Println(ret)
	
	if len(os.Args) < 2 {
		err_message()
	}

	file, err := os.Open(os.Args[1])
	functions.CheckError(err)
	defer file.Close()

	data := make([]byte, pairing.ZrLength()-1)
	noOfBlocks := 0
	NumOfSectors := functions.TotalSectors()
	var EndOfFile bool = false
	var sigmaTotal *pbc.Element

	x := func() {
        defer timeTrack(time.Now(), "x")

		for EndOfFile == false {
			noOfBlocks += 1
			s_n := strconv.Itoa(noOfBlocks)             // id

			sigmaTotal = pairing.NewG1().Set1()

			v := url.Values{}
			v.Set("id", s_n)

			for i := 0; i < NumOfSectors; i++ {
				_, err := file.Read(data)
				if err != nil {
					if err != io.EOF {
						log.Fatal(err)
					}
					EndOfFile = true
					break
				}

				m := pairing.NewZr().SetBytes(data)
				v.Add("m", m.String())
				sigmaTotal = pairing.NewG1().Mul(sigmaTotal, pairing.NewG1().PowZn(u, m))
			}
			
			
			in := []byte(s_n)
			hash := sha256.New()
			hash.Write(in)
			h := pairing.NewG1().SetFromHash(hash.Sum(nil))

			sigma := pairing.NewG1().PowZn(pairing.NewG1().Mul(h, sigmaTotal), sk)
			v.Add("sigma", sigma.String())
			
			s := v.Encode()

			dest := "http://127.0.0.1:8003/upload"
			conttype := "application/x-www-form-urlencoded"
			ret = functions.Connect(dest, s, conttype)
			fmt.Println(ret)
		}
	}
	x()

	functions.FileCrWr("./tmp/noOfBlocks", []byte(strconv.Itoa(noOfBlocks)))

	y := func() {
		defer timeTrack(time.Now(), "y")

		hsh := "\nconnecting to the server http://127.0.0.1:8003/hash...\n"
		hshtype := ""
		hshloc := "http://127.0.0.1:8003/hash"
		ret = functions.Connect(hshloc, hsh, hshtype)
		fmt.Println(ret)

		h_ret, flag := pairing.NewG1().SetString(strings.Split(ret, "||")[0], 10)
		if flag != true {
			log.Fatalf("h_ret not set")
		}
		// Remaining: check whether h_ret equal to h_concat
		signature := pairing.NewG1().PowZn(h_ret, sk)
		ret = ret + "||" + signature.String()

		tx1, err := instContract.SetSign(auth, ret)
		functions.CheckError(err)
		_, err = bind.WaitMined(ctx, connection, tx1)
		functions.CheckError(err)
	}
	y()
}
