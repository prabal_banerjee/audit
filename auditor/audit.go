package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"net/http"
	"strings"
	"math/rand"
	"time"
	"crypto/sha256"
	"net/url"
	"log"
	"context"
	"encoding/json"
	"github.com/Nik-U/pbc"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"bitbucket.com/audit/other/func"
)

var NumOfSectors int = functions.TotalSectors()
var NumOfBlocks int
var SectorsInLastBlock int

var pairing *pbc.Pairing
var g *pbc.Element
var u *pbc.Element
var pk *pbc.Element
var SigmaAggr *pbc.Element
var MUAggr *pbc.Element
var hPownuAggr *pbc.Element
var ProduPowmuj *pbc.Element

var avgT time.Duration
var counter int
func timeTrack(start time.Time, name string) {
        elapsed := time.Since(start)
        fmt.Printf("function %s took %s\n", name, elapsed)
        avgT += elapsed
        counter +=1
        if counter == 10 {
        	avgT /= 10
        	fmt.Printf("\naverage function time %s\n", avgT)
        }
}

func ReadVariables(){
	NumOfSectors = functions.TotalSectors()
	NumOfBlocks = functions.NoofBlocks()
}

func setAggr() {
	SigmaAggr = pairing.NewG1().Set1()
	MUAggr = pairing.NewZr().Set0()
	hPownuAggr = pairing.NewG1().Set1()
}

func random(min, max int) int {
	return rand.Intn(max-min)+min
}

type SharedParams struct {
	Params string 		// group parameter
	G []byte 			// shared G 
	U []byte
	PK []byte
	AggrSigma []byte
	AggrMU []byte
	//structVars Shared
	I [][]byte
	NU[][]byte
}

var Icopy [][]byte
var NUcopy [][]byte

func ContractVars() (*Audit, *bind.TransactOpts, *ethclient.Client) {
	gethPath, nodeKey := functions.GethPathAndKey()
    
    addrByte, err := ioutil.ReadFile("./tmp/contractInfo/contract-address")
	functions.CheckError(err)
    
    connection, err := ethclient.Dial(gethPath)
    functions.CheckError(err)
    fmt.Println("connection Returned: ", connection)
    
    instContract, err := NewAudit(common.HexToAddress(string(addrByte)), connection)
    functions.CheckError(err)
    fmt.Println("instContract Returned: ", instContract)
    
	auth, err := bind.NewTransactor(strings.NewReader(nodeKey), "asdfgh")
	functions.CheckError(err)

	return instContract, auth, connection
}

func verification(sigma *pbc.Element, hPownu *pbc.Element, Sum_mu_j *pbc.Element) {
	/* Audit verification eqn : e(sigma, g) = e(H(i)^nu . u^mu, v) */
	lhs := pairing.NewGT().Pair(sigma, g)
	rhs := pairing.NewGT().Pair(pairing.NewG1().Mul(hPownu, pairing.NewG1().PowZn(u, Sum_mu_j)), pk)

	if lhs.Equals(rhs) {
		fmt.Println("Audit passed successfully")
    } else {
        fmt.Println("Audit failed!!")
    }
}

func queryServer() {
	noOfBlocks := functions.NoofBlocks()
	
	// Let the query be (i,nu)
	// Generate i randomly between [1,noOfBlocks]
	mrand := random(1, noOfBlocks+1)
	fmt.Println(mrand)
	
	// Calculate h = H(i)
	i := []byte(strconv.Itoa(mrand))
	hash := sha256.New()
	hash.Write(i)
	h := pairing.NewG1().SetFromHash(hash.Sum(nil)) 
	
	nu := pairing.NewZr().Rand()
	fmt.Println("nu :", nu)

	hPownu := pairing.NewG1().PowZn(h, nu)
	hPownuAggr = pairing.NewG1().Mul(hPownuAggr, hPownu)
	// Need (sigma,mu) from server, give (i,nu)
	v := url.Values{}
	v.Set("i", string(i))
	v.Add("nu", nu.String())
	s := v.Encode()

	client := &http.Client{}
	r, err := http.NewRequest("POST", "http://127.0.0.1:8003/query", strings.NewReader(s))
	functions.CheckError(err)
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(s)))
	
	resp, err := client.Do(r)
	functions.CheckError(err)
	defer resp.Body.Close()

	var strmsg string
	if resp.StatusCode == http.StatusOK {
		message, err := ioutil.ReadAll(resp.Body)
		functions.CheckError(err)
		strmsg = string(message)
	}

	mu := make([]*pbc.Element, NumOfSectors)
	Sum_mu_j := pairing.NewZr().Set0()

	itr, err := strconv.Atoi(strings.Split(strmsg, "||")[0])
	functions.CheckError(err)

	sigma, flag := pairing.NewG1().SetString(strings.Split(strmsg, "||")[1], 10)
	if flag != true {
		log.Fatalf("sigma not set")
	}

	for j := 0; j < itr; j++ {
		mu[j], flag = pairing.NewZr().SetString(strings.Split(strmsg, "||")[j+2], 10)
		if flag != true {
			log.Fatalf("mu_j not set")
		}
		Sum_mu_j = pairing.NewZr().Add(Sum_mu_j, mu[j])
	}
	//sigma := pairing.NewG1().PowZn(sign, nu)
	//ProduPowmuj = pairing.NewG1().PowZn(u, Sum_mu_j)

	verification(sigma, hPownu, Sum_mu_j)

    /*msg := Shared {
    	I: i,
    	NU: nu.Bytes(),
	}*/

	//fmt.Println(nu.Bytes())
	//structVar = append(structVar, msg)
	//structVar.I = append(structVar.I, i)
	//structVar.NU = append(structVar.NU, nu.Bytes())
	Icopy = append(Icopy, i)
	NUcopy = append(NUcopy, nu.Bytes())

	SigmaAggr = pairing.NewG1().Mul(SigmaAggr, sigma)
	MUAggr = pairing.NewZr().Add(MUAggr, Sum_mu_j)

}

func main() {
	instContract, auth, connection := ContractVars()

	paramsByte, GetGVal, GetUVal, err := instContract.GetParamsGU(nil)
	functions.CheckError(err)

	GetPKOVal, err := instContract.GetPKO(nil)
	functions.CheckError(err)
	
	pairing, err = pbc.NewPairingFromString(string(paramsByte))
	functions.CheckError(err)

	g = pairing.NewG1().SetBytes(GetGVal)
	u = pairing.NewG1().SetBytes(GetUVal)
	pk = pairing.NewG1().SetBytes(GetPKOVal)

	//rand.New(rand.NewSource(99))
	//rand.Seed(42)
	rand.Seed(time.Now().Unix())
	
	setAggr()
	
	for j := 0; j < 7; j++ {
		x := func() {
        	defer timeTrack(time.Now(), "x")
			queryServer()
		}
		x()
	}
	
	y := func() {
		defer timeTrack(time.Now(), "y")
		verification(SigmaAggr, hPownuAggr, MUAggr)
	}
	y()

	//fmt.Println(NUcopy[5])

	//iAndnuShared, err := json.Marshal(structVar)
	//functions.CheckError(err)

	structParamsVar := SharedParams{	
        	Params: string(paramsByte),
        	G: GetGVal,
        	U: GetUVal,
        	PK: GetPKOVal,
        	AggrSigma: SigmaAggr.Bytes(),
        	AggrMU: MUAggr.Bytes(),
        	//structVars: structVar,
        	I: Icopy,
        	NU: NUcopy,
	}

	byteShared, err := json.Marshal(structParamsVar)
	functions.CheckError(err)
	
	z := func() {
		defer timeTrack(time.Now(), "z")
		ctx := context.Background()
	    
	    // Send audit query	
		tx, err := instContract.SendAudit(auth, []byte(byteShared))
		functions.CheckError(err)
		_, err = bind.WaitMined(ctx, connection, tx)
		functions.CheckError(err)
		
		//Check if audit passed
		auditFlag, err := instContract.QueryAudit(nil)
		functions.CheckError(err)	
		fmt.Println("Audit Query Returned ", auditFlag)
	}
	z()
}
