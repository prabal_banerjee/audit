import matplotlib.pyplot as plt 

# x-coordinates of left sides of bars  
left = [1, 2, 3, 4, 5, 6] 
  
# heights of bars 
height = [.074122649, 0.630108248, 6.216906635, 61.844308565,
		309.391804855, 617.434609612]

# labels for bars 
tick_label = ['1KB', '10KB', '100KB', '1MB', '5MB', '10MB']

# plotting a histogram 
plt.bar(left, height, color = 'green', tick_label = tick_label,
	 width = 0.4) 

# x-axis label 
plt.xlabel('Size of File') 
# frequency label 
plt.ylabel('Owner-Server communication') 
# plot title 
plt.title('') 

# function to show the plot 
plt.show() 
