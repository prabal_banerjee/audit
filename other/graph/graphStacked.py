import numpy as np
import matplotlib.pyplot as plt


N = 3
menMeans = (62.465332689, 14.057949, .70599872)
womenMeans = (5.01579659, 0, 6.394123589)
menStd = (2, 3, 4, 1, 2)
womenStd = (3, 5, 2, 3, 3)
ind = np.arange(N)    # the x locations for the groups
width = 0.35       # the width of the bars: can also be len(x) sequence

p1 = plt.bar(ind, menMeans, width)
p2 = plt.bar(ind, womenMeans, width,
             bottom=menMeans)

plt.yscale('log')

plt.ylabel('Time (in Seconds)')
#plt.xlabel('')
plt.title('File Size = 1MB, Number of Queries = 10')
plt.xticks(ind, ('Owner Computation', 'Server Computation', 'Auditor Computation'))
#plt.yticks(np.arange(0, 71, 5))
plt.legend((p1[0], p2[0]), ('Time for Computation', 'Blockchain Call Time'))

plt.show()