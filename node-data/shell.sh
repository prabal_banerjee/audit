#!/bin/sh

geth1="geth console --datadir node1 --networkid 1729"
geth2="geth console --datadir node2 --networkid 1729 --port 30304 --bootnodes enode://0e1a1250bce3e603e21d3d2b96a09740eade11043bf8b0eb1fc2fa3de5cba2fc790971fa77bc6e7a5b439d23282e56b5bdc8a8cd9ff6940d5b8216e97d267403@127.0.0.1:30303  --rpc --rpccorsdomain "http://localhost:8000" --unlock 0x9d2d449fa0d6423ca09dbbada4064282881952dd --password password.txt --mine --minerthreads=1"

# if you want to use xterm
#xterm -title "Node1" -hold -e ${geth1}| xterm -title "Node2" -hold -e ${geth2}

# if you want to use terminal
gnome-terminal --title="Node1" -p -x ${geth1}| gnome-terminal --title="Node2" -p -x ${geth2}
#gnome-terminal -- /bin/bash - '${geth1};read'

# if you want to use xfce-terminal
#xfce4-terminal -H --title="Node1" -x ${geth1}| xfce4-terminal -H --title="Node2" -x ${geth2}
