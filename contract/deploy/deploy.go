package main

import (
	"fmt"
	"strings"
	"context"	
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/Nik-U/pbc"
	"bitbucket.com/audit/func"
)

func CommContract(paramsByte []byte) {
	gethPath, nodeKey := functions.GethPathAndKey()
    
    connection, err := ethclient.Dial(gethPath)
    functions.CheckError(err)
    fmt.Println("connection Returned: ", connection)
    
	auth, err := bind.NewTransactor(strings.NewReader(nodeKey), "asdfgh")
	functions.CheckError(err)

    addr, tx, instContract, err := DeployAudit(auth, connection)
	functions.CheckError(err)
    fmt.Println("addr Returned: ", addr)
    fmt.Println("instContract Returned: ", instContract)
    
    // Write contract address to file for other app reference
	functions.FileCrWr("./tmp/contractInfo/contract-address", []byte(addr.Hex()))

    ctx := context.Background()
    _, err = bind.WaitMined(ctx, connection, tx)
    functions.CheckError(err)

    pairing, err := pbc.NewPairingFromString(string(paramsByte))
	functions.CheckError(err)
	g := pairing.NewG1().Rand()
	u := pairing.NewG1().Rand()

	tx1, err := instContract.SetParamsGU(auth, paramsByte, g.Bytes(), u.Bytes())
	functions.CheckError(err)
	//fmt.Println("\nTxn1 hash is: ", tx1.Hash())
	_, err = bind.WaitMined(ctx, connection, tx1)
	functions.CheckError(err)
}

func main() {
	// create ./tmp/ folder for storing secrets information of parties and public parameters 
	functions.Createtmp()

	// In a real application, generate this once and publish it
	params := pbc.GenerateA(160, 512)

	CommContract([]byte(params.String()))

}
