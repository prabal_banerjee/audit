// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package main

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = abi.U256
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AuditABI is the input ABI used to generate the binding from.
const AuditABI = "[{\"constant\":true,\"inputs\":[],\"name\":\"getPKS\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getPKO\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"queryAudit\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"input\",\"type\":\"string\"}],\"name\":\"setSign\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getParamsGU\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes\"},{\"name\":\"\",\"type\":\"bytes\"},{\"name\":\"\",\"type\":\"bytes\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"input1\",\"type\":\"bytes\"},{\"name\":\"input2\",\"type\":\"bytes\"},{\"name\":\"input3\",\"type\":\"bytes\"}],\"name\":\"setParamsGU\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"input\",\"type\":\"bytes\"}],\"name\":\"sendAudit\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"x\",\"type\":\"uint256\"}],\"name\":\"set\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"get\",\"outputs\":[{\"name\":\"retVal\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"input\",\"type\":\"bytes\"}],\"name\":\"setPKS\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"input\",\"type\":\"bytes\"}],\"name\":\"setPKO\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"}]"

// AuditBin is the compiled bytecode used for deploying new contracts.
const AuditBin = `0x608060405234801561001057600080fd5b506001805460ff19169055610beb8061002a6000396000f3fe608060405234801561001057600080fd5b50600436106100c6576000357c0100000000000000000000000000000000000000000000000000000000900480633f47283e1161008e5780633f47283e1461036057806356d0022f1461051457806360fe47b1146105ba5780636d4ce63c146105d757806377c4e362146105f15780638ce47f7814610697576100c6565b8063076ab3e5146100cb57806313250ddf146101485780631afbca031461015057806331fbbae21461016c5780633e06103414610214575b600080fd5b6100d361073d565b6040805160208082528351818301528351919283929083019185019080838360005b8381101561010d5781810151838201526020016100f5565b50505050905090810190601f16801561013a5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6100d36107d4565b610158610835565b604080519115158252519081900360200190f35b6102126004803603602081101561018257600080fd5b81019060208101813564010000000081111561019d57600080fd5b8201836020820111156101af57600080fd5b803590602001918460018302840111640100000000831117156101d157600080fd5b91908080601f01602080910402602001604051908101604052809392919081815260200183838082843760009201919091525092955061083e945050505050565b005b61021c610855565b60405180806020018060200180602001848103845287818151815260200191508051906020019080838360005b83811015610261578181015183820152602001610249565b50505050905090810190601f16801561028e5780820380516001836020036101000a031916815260200191505b50848103835286518152865160209182019188019080838360005b838110156102c15781810151838201526020016102a9565b50505050905090810190601f1680156102ee5780820380516001836020036101000a031916815260200191505b50848103825285518152855160209182019187019080838360005b83811015610321578181015183820152602001610309565b50505050905090810190601f16801561034e5780820380516001836020036101000a031916815260200191505b50965050505050505060405180910390f35b6102126004803603606081101561037657600080fd5b81019060208101813564010000000081111561039157600080fd5b8201836020820111156103a357600080fd5b803590602001918460018302840111640100000000831117156103c557600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929594936020810193503591505064010000000081111561041857600080fd5b82018360208201111561042a57600080fd5b8035906020019184600183028401116401000000008311171561044c57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929594936020810193503591505064010000000081111561049f57600080fd5b8201836020820111156104b157600080fd5b803590602001918460018302840111640100000000831117156104d357600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610a16945050505050565b6102126004803603602081101561052a57600080fd5b81019060208101813564010000000081111561054557600080fd5b82018360208201111561055757600080fd5b8035906020019184600183028401116401000000008311171561057957600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610a57945050505050565b610212600480360360208110156105d057600080fd5b5035610adb565b6105df610ae0565b60408051918252519081900360200190f35b6102126004803603602081101561060757600080fd5b81019060208101813564010000000081111561062257600080fd5b82018360208201111561063457600080fd5b8035906020019184600183028401116401000000008311171561065657600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610ae6945050505050565b610212600480360360208110156106ad57600080fd5b8101906020810181356401000000008111156106c857600080fd5b8201836020820111156106da57600080fd5b803590602001918460018302840111640100000000831117156106fc57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610af9945050505050565b60068054604080516020601f60026000196101006001881615020190951694909404938401819004810282018101909252828152606093909290918301828280156107c95780601f1061079e576101008083540402835291602001916107c9565b820191906000526020600020905b8154815290600101906020018083116107ac57829003601f168201915b505050505090505b90565b60058054604080516020601f60026000196101006001881615020190951694909404938401819004810282018101909252828152606093909290918301828280156107c95780601f1061079e576101008083540402835291602001916107c9565b60015460ff1690565b8051610851906007906020840190610b08565b5050565b60028054604080516020601f6000196101006001871615020190941685900493840181900481028201810190925282815260609384938493919260039260049285918301828280156108e85780601f106108bd576101008083540402835291602001916108e8565b820191906000526020600020905b8154815290600101906020018083116108cb57829003601f168201915b5050855460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152959850879450925084019050828280156109765780601f1061094b57610100808354040283529160200191610976565b820191906000526020600020905b81548152906001019060200180831161095957829003601f168201915b5050845460408051602060026001851615610100026000190190941693909304601f810184900484028201840190925281815295975086945092508401905082828015610a045780601f106109d957610100808354040283529160200191610a04565b820191906000526020600020905b8154815290600101906020018083116109e757829003601f168201915b50505050509050925092509250909192565b8251610a29906002906020860190610b08565b508151610a3d906003906020850190610b08565b508051610a51906004906020840190610b08565b50505050565b610a5f610b86565b81516000906020016001838286856009600019f11591508115610a7e57fe5b82517fff00000000000000000000000000000000000000000000000000000000000000167f31000000000000000000000000000000000000000000000000000000000000001415610a51576001805460ff19168117905550505050565b600055565b60005490565b8051610851906006906020840190610b08565b80516108519060059060208401905b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10610b4957805160ff1916838001178555610b76565b82800160010185558215610b76579182015b82811115610b76578251825591602001919060010190610b5b565b50610b82929150610ba5565b5090565b6020604051908101604052806001906020820280388339509192915050565b6107d191905b80821115610b825760008155600101610bab56fea165627a7a72305820c89f0f9e998c916b6726e820e78545b06efa5b6ba4762a6a02529f11312cf50a0029`

// DeployAudit deploys a new Ethereum contract, binding an instance of Audit to it.
func DeployAudit(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Audit, error) {
	parsed, err := abi.JSON(strings.NewReader(AuditABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(AuditBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Audit{AuditCaller: AuditCaller{contract: contract}, AuditTransactor: AuditTransactor{contract: contract}, AuditFilterer: AuditFilterer{contract: contract}}, nil
}

// Audit is an auto generated Go binding around an Ethereum contract.
type Audit struct {
	AuditCaller     // Read-only binding to the contract
	AuditTransactor // Write-only binding to the contract
	AuditFilterer   // Log filterer for contract events
}

// AuditCaller is an auto generated read-only Go binding around an Ethereum contract.
type AuditCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AuditTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AuditTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AuditFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AuditFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AuditSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AuditSession struct {
	Contract     *Audit            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AuditCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AuditCallerSession struct {
	Contract *AuditCaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// AuditTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AuditTransactorSession struct {
	Contract     *AuditTransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AuditRaw is an auto generated low-level Go binding around an Ethereum contract.
type AuditRaw struct {
	Contract *Audit // Generic contract binding to access the raw methods on
}

// AuditCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AuditCallerRaw struct {
	Contract *AuditCaller // Generic read-only contract binding to access the raw methods on
}

// AuditTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AuditTransactorRaw struct {
	Contract *AuditTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAudit creates a new instance of Audit, bound to a specific deployed contract.
func NewAudit(address common.Address, backend bind.ContractBackend) (*Audit, error) {
	contract, err := bindAudit(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Audit{AuditCaller: AuditCaller{contract: contract}, AuditTransactor: AuditTransactor{contract: contract}, AuditFilterer: AuditFilterer{contract: contract}}, nil
}

// NewAuditCaller creates a new read-only instance of Audit, bound to a specific deployed contract.
func NewAuditCaller(address common.Address, caller bind.ContractCaller) (*AuditCaller, error) {
	contract, err := bindAudit(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AuditCaller{contract: contract}, nil
}

// NewAuditTransactor creates a new write-only instance of Audit, bound to a specific deployed contract.
func NewAuditTransactor(address common.Address, transactor bind.ContractTransactor) (*AuditTransactor, error) {
	contract, err := bindAudit(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AuditTransactor{contract: contract}, nil
}

// NewAuditFilterer creates a new log filterer instance of Audit, bound to a specific deployed contract.
func NewAuditFilterer(address common.Address, filterer bind.ContractFilterer) (*AuditFilterer, error) {
	contract, err := bindAudit(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AuditFilterer{contract: contract}, nil
}

// bindAudit binds a generic wrapper to an already deployed contract.
func bindAudit(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(AuditABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Audit *AuditRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Audit.Contract.AuditCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Audit *AuditRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Audit.Contract.AuditTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Audit *AuditRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Audit.Contract.AuditTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Audit *AuditCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Audit.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Audit *AuditTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Audit.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Audit *AuditTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Audit.Contract.contract.Transact(opts, method, params...)
}

// Get is a free data retrieval call binding the contract method 0x6d4ce63c.
//
// Solidity: function get() constant returns(retVal uint256)
func (_Audit *AuditCaller) Get(opts *bind.CallOpts) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _Audit.contract.Call(opts, out, "get")
	return *ret0, err
}

// Get is a free data retrieval call binding the contract method 0x6d4ce63c.
//
// Solidity: function get() constant returns(retVal uint256)
func (_Audit *AuditSession) Get() (*big.Int, error) {
	return _Audit.Contract.Get(&_Audit.CallOpts)
}

// Get is a free data retrieval call binding the contract method 0x6d4ce63c.
//
// Solidity: function get() constant returns(retVal uint256)
func (_Audit *AuditCallerSession) Get() (*big.Int, error) {
	return _Audit.Contract.Get(&_Audit.CallOpts)
}

// GetPKO is a free data retrieval call binding the contract method 0x13250ddf.
//
// Solidity: function getPKO() constant returns(bytes)
func (_Audit *AuditCaller) GetPKO(opts *bind.CallOpts) ([]byte, error) {
	var (
		ret0 = new([]byte)
	)
	out := ret0
	err := _Audit.contract.Call(opts, out, "getPKO")
	return *ret0, err
}

// GetPKO is a free data retrieval call binding the contract method 0x13250ddf.
//
// Solidity: function getPKO() constant returns(bytes)
func (_Audit *AuditSession) GetPKO() ([]byte, error) {
	return _Audit.Contract.GetPKO(&_Audit.CallOpts)
}

// GetPKO is a free data retrieval call binding the contract method 0x13250ddf.
//
// Solidity: function getPKO() constant returns(bytes)
func (_Audit *AuditCallerSession) GetPKO() ([]byte, error) {
	return _Audit.Contract.GetPKO(&_Audit.CallOpts)
}

// GetPKS is a free data retrieval call binding the contract method 0x076ab3e5.
//
// Solidity: function getPKS() constant returns(bytes)
func (_Audit *AuditCaller) GetPKS(opts *bind.CallOpts) ([]byte, error) {
	var (
		ret0 = new([]byte)
	)
	out := ret0
	err := _Audit.contract.Call(opts, out, "getPKS")
	return *ret0, err
}

// GetPKS is a free data retrieval call binding the contract method 0x076ab3e5.
//
// Solidity: function getPKS() constant returns(bytes)
func (_Audit *AuditSession) GetPKS() ([]byte, error) {
	return _Audit.Contract.GetPKS(&_Audit.CallOpts)
}

// GetPKS is a free data retrieval call binding the contract method 0x076ab3e5.
//
// Solidity: function getPKS() constant returns(bytes)
func (_Audit *AuditCallerSession) GetPKS() ([]byte, error) {
	return _Audit.Contract.GetPKS(&_Audit.CallOpts)
}

// GetParamsGU is a free data retrieval call binding the contract method 0x3e061034.
//
// Solidity: function getParamsGU() constant returns(bytes, bytes, bytes)
func (_Audit *AuditCaller) GetParamsGU(opts *bind.CallOpts) ([]byte, []byte, []byte, error) {
	var (
		ret0 = new([]byte)
		ret1 = new([]byte)
		ret2 = new([]byte)
	)
	out := &[]interface{}{
		ret0,
		ret1,
		ret2,
	}
	err := _Audit.contract.Call(opts, out, "getParamsGU")
	return *ret0, *ret1, *ret2, err
}

// GetParamsGU is a free data retrieval call binding the contract method 0x3e061034.
//
// Solidity: function getParamsGU() constant returns(bytes, bytes, bytes)
func (_Audit *AuditSession) GetParamsGU() ([]byte, []byte, []byte, error) {
	return _Audit.Contract.GetParamsGU(&_Audit.CallOpts)
}

// GetParamsGU is a free data retrieval call binding the contract method 0x3e061034.
//
// Solidity: function getParamsGU() constant returns(bytes, bytes, bytes)
func (_Audit *AuditCallerSession) GetParamsGU() ([]byte, []byte, []byte, error) {
	return _Audit.Contract.GetParamsGU(&_Audit.CallOpts)
}

// QueryAudit is a free data retrieval call binding the contract method 0x1afbca03.
//
// Solidity: function queryAudit() constant returns(bool)
func (_Audit *AuditCaller) QueryAudit(opts *bind.CallOpts) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Audit.contract.Call(opts, out, "queryAudit")
	return *ret0, err
}

// QueryAudit is a free data retrieval call binding the contract method 0x1afbca03.
//
// Solidity: function queryAudit() constant returns(bool)
func (_Audit *AuditSession) QueryAudit() (bool, error) {
	return _Audit.Contract.QueryAudit(&_Audit.CallOpts)
}

// QueryAudit is a free data retrieval call binding the contract method 0x1afbca03.
//
// Solidity: function queryAudit() constant returns(bool)
func (_Audit *AuditCallerSession) QueryAudit() (bool, error) {
	return _Audit.Contract.QueryAudit(&_Audit.CallOpts)
}

// SendAudit is a paid mutator transaction binding the contract method 0x56d0022f.
//
// Solidity: function sendAudit(input bytes) returns()
func (_Audit *AuditTransactor) SendAudit(opts *bind.TransactOpts, input []byte) (*types.Transaction, error) {
	return _Audit.contract.Transact(opts, "sendAudit", input)
}

// SendAudit is a paid mutator transaction binding the contract method 0x56d0022f.
//
// Solidity: function sendAudit(input bytes) returns()
func (_Audit *AuditSession) SendAudit(input []byte) (*types.Transaction, error) {
	return _Audit.Contract.SendAudit(&_Audit.TransactOpts, input)
}

// SendAudit is a paid mutator transaction binding the contract method 0x56d0022f.
//
// Solidity: function sendAudit(input bytes) returns()
func (_Audit *AuditTransactorSession) SendAudit(input []byte) (*types.Transaction, error) {
	return _Audit.Contract.SendAudit(&_Audit.TransactOpts, input)
}

// Set is a paid mutator transaction binding the contract method 0x60fe47b1.
//
// Solidity: function set(x uint256) returns()
func (_Audit *AuditTransactor) Set(opts *bind.TransactOpts, x *big.Int) (*types.Transaction, error) {
	return _Audit.contract.Transact(opts, "set", x)
}

// Set is a paid mutator transaction binding the contract method 0x60fe47b1.
//
// Solidity: function set(x uint256) returns()
func (_Audit *AuditSession) Set(x *big.Int) (*types.Transaction, error) {
	return _Audit.Contract.Set(&_Audit.TransactOpts, x)
}

// Set is a paid mutator transaction binding the contract method 0x60fe47b1.
//
// Solidity: function set(x uint256) returns()
func (_Audit *AuditTransactorSession) Set(x *big.Int) (*types.Transaction, error) {
	return _Audit.Contract.Set(&_Audit.TransactOpts, x)
}

// SetPKO is a paid mutator transaction binding the contract method 0x8ce47f78.
//
// Solidity: function setPKO(input bytes) returns()
func (_Audit *AuditTransactor) SetPKO(opts *bind.TransactOpts, input []byte) (*types.Transaction, error) {
	return _Audit.contract.Transact(opts, "setPKO", input)
}

// SetPKO is a paid mutator transaction binding the contract method 0x8ce47f78.
//
// Solidity: function setPKO(input bytes) returns()
func (_Audit *AuditSession) SetPKO(input []byte) (*types.Transaction, error) {
	return _Audit.Contract.SetPKO(&_Audit.TransactOpts, input)
}

// SetPKO is a paid mutator transaction binding the contract method 0x8ce47f78.
//
// Solidity: function setPKO(input bytes) returns()
func (_Audit *AuditTransactorSession) SetPKO(input []byte) (*types.Transaction, error) {
	return _Audit.Contract.SetPKO(&_Audit.TransactOpts, input)
}

// SetPKS is a paid mutator transaction binding the contract method 0x77c4e362.
//
// Solidity: function setPKS(input bytes) returns()
func (_Audit *AuditTransactor) SetPKS(opts *bind.TransactOpts, input []byte) (*types.Transaction, error) {
	return _Audit.contract.Transact(opts, "setPKS", input)
}

// SetPKS is a paid mutator transaction binding the contract method 0x77c4e362.
//
// Solidity: function setPKS(input bytes) returns()
func (_Audit *AuditSession) SetPKS(input []byte) (*types.Transaction, error) {
	return _Audit.Contract.SetPKS(&_Audit.TransactOpts, input)
}

// SetPKS is a paid mutator transaction binding the contract method 0x77c4e362.
//
// Solidity: function setPKS(input bytes) returns()
func (_Audit *AuditTransactorSession) SetPKS(input []byte) (*types.Transaction, error) {
	return _Audit.Contract.SetPKS(&_Audit.TransactOpts, input)
}

// SetParamsGU is a paid mutator transaction binding the contract method 0x3f47283e.
//
// Solidity: function setParamsGU(input1 bytes, input2 bytes, input3 bytes) returns()
func (_Audit *AuditTransactor) SetParamsGU(opts *bind.TransactOpts, input1 []byte, input2 []byte, input3 []byte) (*types.Transaction, error) {
	return _Audit.contract.Transact(opts, "setParamsGU", input1, input2, input3)
}

// SetParamsGU is a paid mutator transaction binding the contract method 0x3f47283e.
//
// Solidity: function setParamsGU(input1 bytes, input2 bytes, input3 bytes) returns()
func (_Audit *AuditSession) SetParamsGU(input1 []byte, input2 []byte, input3 []byte) (*types.Transaction, error) {
	return _Audit.Contract.SetParamsGU(&_Audit.TransactOpts, input1, input2, input3)
}

// SetParamsGU is a paid mutator transaction binding the contract method 0x3f47283e.
//
// Solidity: function setParamsGU(input1 bytes, input2 bytes, input3 bytes) returns()
func (_Audit *AuditTransactorSession) SetParamsGU(input1 []byte, input2 []byte, input3 []byte) (*types.Transaction, error) {
	return _Audit.Contract.SetParamsGU(&_Audit.TransactOpts, input1, input2, input3)
}

// SetSign is a paid mutator transaction binding the contract method 0x31fbbae2.
//
// Solidity: function setSign(input string) returns()
func (_Audit *AuditTransactor) SetSign(opts *bind.TransactOpts, input string) (*types.Transaction, error) {
	return _Audit.contract.Transact(opts, "setSign", input)
}

// SetSign is a paid mutator transaction binding the contract method 0x31fbbae2.
//
// Solidity: function setSign(input string) returns()
func (_Audit *AuditSession) SetSign(input string) (*types.Transaction, error) {
	return _Audit.Contract.SetSign(&_Audit.TransactOpts, input)
}

// SetSign is a paid mutator transaction binding the contract method 0x31fbbae2.
//
// Solidity: function setSign(input string) returns()
func (_Audit *AuditTransactorSession) SetSign(input string) (*types.Transaction, error) {
	return _Audit.Contract.SetSign(&_Audit.TransactOpts, input)
}
