pragma solidity ^0.5.0;
// pragma solidity >=0.4.22 <0.6.0;

contract Audit {
    //a simple unsigned int variable
    uint private storedData;
    bool private auditPass;
    bytes storedParams;
    bytes storedG;
    bytes storedU;
    bytes storedPKO;
    bytes storedPKS;
    string storedSign;

    /*struct PBCvars { // Struct
        bytes storedParams;
        bytes storedG;
        bytes storedU;
    }

    struct PKs {
    	bytes storedPKO;
    	bytes storedPKS;
    }*/
    
    //func to set the value of the var
    function set (uint x) public {
        storedData = x;
    }

    function setParamsGU (bytes memory input1, bytes memory input2, bytes memory input3) public {
        storedParams = input1;
        storedG = input2;
        storedU = input3;
    }
    
    function setPKO (bytes memory input) public {
        storedPKO = input;
    }
    
    function setPKS (bytes memory input) public {
        storedPKS = input;
    }

    function setSign (string memory input) public {
        storedSign = input;
    }

    //func to get the value of the var
    function get() public view returns (uint retVal) {
        return storedData;
    }

    function getParamsGU() public view returns (bytes memory, bytes memory, bytes memory) {
        return (storedParams, storedG, storedU);
    }

    function getPKO() public view returns (bytes memory) {
        return storedPKO;
    }

    function getPKS() public view returns (bytes memory) {
        return storedPKS;
    }

    //func to do modular exp using pre-compiles
    // function modExp(uint256 base, uint256 e, uint256 m)  public pure returns (uint256[1] memory output) {
    //     //load args to memory
    //     uint256[6] memory input;
    //     input[0] = 32;
    //     input[1] = 32;
    //     input[2] = 32;
    //     input[3] = base;
    //     input[4] = e;
    //     input[5] = m;

    //     //output[0] = 333;

    //     //make the assembly call
    //     assembly {
    //         //call(gasLimit, address, gas, input, inputSize, output, outputSize)
    //         if iszero(call(not(0), 0x05, 0, input, 0xc0, output, 0x20)) {
    //             revert(0,0)
    //         }
    //     }
    // }
    
    constructor() public {
        auditPass = false;
    }
        
    //func to test newly written pre-compile
    function sendAudit(bytes memory input) public {
        // sample byte array : 68,81,162,21,71,37,14,225,210,46,25,54,205,143,240,59,44,78,168,124,0,75,216,111,164,210,131,234,78,164,239,230,220,209,93,166,44,239,60,175,224,68,207,128,79,53,142,103,12,115,129,197,95,12,99,84,237,188,230,12,179,129,123,206,21,52,160,128,235,247,94,118,146,210,79,52,134,145,221,26,30,3,153,81,12,77,134,67,177,209,99,242,74,75,55,217,60,106,21,223,163,164,239,157,26,190,249,32,204,110,104,224,56,176,134,173,140,34,19,247,177,63,115,141,18,239,219,194
        //uint8[559] memory input = [123,34,71,34,58,34,75,117,84,52,84,122,118,43,112,73,73,75,76,50,87,43,65,73,121,49,88,76,101,52,81,117,109,86,50,70,118,73,115,43,122,56,119,114,89,77,101,86,49,65,75,71,83,121,87,69,47,47,116,56,49,56,47,82,78,111,109,90,50,48,108,55,110,67,119,97,104,107,113,105,50,114,77,112,113,88,78,48,78,80,51,119,75,119,83,86,79,51,105,107,88,116,104,52,84,77,99,88,98,90,90,55,48,65,122,111,55,47,81,99,109,49,87,52,78,85,66,56,106,43,79,77,49,120,98,75,101,67,119,84,102,111,105,103,54,56,71,57,89,55,53,71,80,110,82,122,47,114,86,81,97,119,82,101,74,84,109,78,48,81,84,113,67,106,116,53,111,61,34,44,34,80,97,114,97,109,115,34,58,34,116,121,112,101,32,97,92,110,113,32,52,49,57,52,50,52,56,53,55,50,52,50,51,48,49,56,53,54,53,57,53,51,56,54,52,54,56,48,48,52,49,52,48,53,50,48,56,56,57,51,56,53,48,52,52,57,51,53,56,49,57,57,49,52,54,52,51,54,55,49,55,48,57,51,50,48,52,48,54,50,57,52,48,49,50,49,52,53,52,48,55,51,51,57,50,54,50,48,48,53,52,57,53,50,50,54,51,52,50,49,52,56,56,54,56,52,56,53,56,53,51,52,57,49,52,57,52,48,48,56,49,57,48,49,55,57,57,48,55,53,50,48,55,53,53,53,57,52,48,55,52,57,55,53,54,54,48,49,56,48,53,51,53,57,92,110,104,32,53,55,51,57,54,51,57,57,50,50,56,50,54,53,51,57,50,48,50,50,51,53,51,54,56,55,53,54,53,50,55,57,50,56,50,56,56,49,50,56,57,55,51,50,48,51,50,53,55,51,51,48,53,49,54,53,53,55,55,52,48,54,56,49,54,55,51,55,56,50,49,48,57,57,50,50,57,50,52,53,49,48,50,54,49,57,52,50,52,53,50,49,57,52,49,53,50,57,56,49,49,50,52,53,54,48,92,110,114,32,55,51,48,55,53,49,49,54,55,49,49,52,53,57,53,49,56,54,49,52,50,56,50,57,48,48,50,56,53,51,55,51,57,53,49,57,57,53,56,54,49,52,56,48,50,52,51,49,92,110,101,120,112,50,32,49,53,57,92,110,101,120,112,49,32,49,51,56,92,110,115,105,103,110,49,32,49,92,110,115,105,103,110,48,32,45,49,92,110,34,125];
        // bytes memory input = '{"G":"nMUgz1IGykDUM1pY9EJfX4bR8LJZ36iFTopEfvr0/MXIs544dS9+lqdsEcwX++xH/jj2BaJleVG2P61GWuXoShF+6BPrgOfqERPJSy41cRIAHn2a/xsgZ99zrd2uohZKyBisQgWYtWc/j7FbZnb9rlE/bMhTBVyjwvOucpZE7Ec=","Params":"type a\\nq 8291267441863824999580247098094621500796951371844983100574876278276536664503130074691079323826329085257716741141506871853952046658716616291687779274835407\\nh 11346230794520323826834595835819183225478290015159145050382589744473975724310356930121733085382507338152496\\nr 730750818665534535851578973600197997769233793023\\nexp2 159\\nexp1 116\\nsign1 1\\nsign0 -1\\n"}';
        // bytes memory input = "{}";
        //input = [65,81];
        byte[1] memory output;
        bool ret;
        uint256 len = input.length + 32;
        
        assembly {
            ret := iszero(call(not(0), 0x09, 0, input, len, output, 0x01))
        }
        assert(!ret);
        
        if (output[0] == '1') {
            auditPass = true;
        }
    }
    
    function queryAudit() public view returns (bool){
        return auditPass;
    }
    //func to test homestead pre-compile identity
    // function iden(uint256 x)  public returns (uint256[1] memory output) {
    //     uint256[1] memory input;
    //     input[0] = x;

    //     output[0] = 55555;

    //     assembly {
    //         if iszero(call(not(0), 0x04, 0, input, 0x20, output, 0x20)) {
    //             revert(0,0)
    //         }
    //     }
    // }
}
