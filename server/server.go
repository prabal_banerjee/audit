package main

import (
	"fmt"
	"net/http"
	"log"
	"strconv"
	"crypto/sha256"
	"io/ioutil"
	"github.com/Nik-U/pbc"
	"context"
	"strings"
	"time"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"bitbucket.com/audit/other/func"
)

var NumOfSectors int
var NumOfBlocks int
var SectorsInLastBlock int

var pairing *pbc.Pairing
var g *pbc.Element
var u *pbc.Element
var pk *pbc.Element
var sk *pbc.Element

var id []int
var m []string
var sigma []string
var h_con []byte      // storing concatenated h1,...,hn

var avgT time.Duration

func ReadVariables(){
	NumOfSectors = functions.TotalSectors()
	NumOfBlocks = functions.NoofBlocks()
	SectorsInLastBlock = len(m)%NumOfSectors
}

var counter int
func timeTrack(start time.Time, name string) {
        elapsed := time.Since(start)
        fmt.Printf("function %s took %s", name, elapsed)
        avgT += elapsed
        counter +=1
        if counter == 10 {
        	avgT /= 10
        	fmt.Printf("\naverage function time %s", avgT)
        }
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	fmt.Println("\nconnection to the server http://127.0.0.1:8003/ is requested ...\n\nconnection accepted ...\n")
	message := "\nYou are connected to http://127.0.0.1:8003/.\n"
	w.Write([]byte(message))
}
        
func queryHandler(w http.ResponseWriter, r *http.Request) {
	defer timeTrack(time.Now(), "x")
	ReadVariables()
	//fmt.Println(len(m))
	r.ParseForm()
	
	fmt.Println("\nquery index i : ", r.FormValue("i"))
	i , err := strconv.Atoi(r.FormValue("i"))
	functions.CheckError(err)
	
	fmt.Println("nu (\u03BD): ", r.FormValue("nu"))

	nu, _ := pairing.NewZr().SetString(r.FormValue("nu"), 10)
	
	var itr int
	switch i {
	case NumOfBlocks:
		itr = SectorsInLastBlock
	default:
		itr = NumOfSectors
	}
	s_itr := strconv.Itoa(itr)
	var muConcat string

	for j := 0; j < itr; j++ {
		mij, _ := pairing.NewZr().SetString(m[(i-1)*NumOfSectors+j], 10)
		mu := pairing.NewZr().Mul(nu, mij)
		//fmt.Printf("mu_%d (\u03BC): %s\n", j, mu.String())
		muConcat = muConcat + "||" + mu.String()
	}


	sigmai, flag := pairing.NewG1().SetString(sigma[i-1], 10)
	if flag != true {
		log.Fatalf("sigma not set")
	}
	sigmaiPownu := pairing.NewG1().PowZn(sigmai, nu)
	//fmt.Println(sigmaiPownu)

	fmt.Fprintf(w, s_itr+"||"+sigmaiPownu.String()+muConcat)
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	
	//fmt.Println("Got new POST(block) from owner\n")

	// NOTE: Invoke ParseForm or ParseMultipartForm before reading form values
	r.ParseForm()
	/*
          Loops through r.Form object to print (id, m_id, sigma_id) 
          pairs of url-encoded data. Note that these include all data           
          sent through request url and request body
    */

	//fmt.Println("id : ", r.FormValue("id"))
	//id_i , err := strconv.Atoi(r.FormValue("id"))
	//functions.CheckError(err)
	//id = append(id, id_i)
	
	var mAndsigma string
	for i := 0; i < len(r.PostForm["m"]); i++ {	
			fmt.Println("m : ", r.PostForm["m"][i])	

		m = append(m, r.PostForm["m"][i])
		mAndsigma += r.PostForm["m"][i]
	}
	
	fmt.Println("sigma : ", r.FormValue("sigma"), "\n")
	sigma = append(sigma, r.FormValue("sigma"))

	mAndsigma += r.FormValue("sigma")
	hash := sha256.New()
	hash.Write([]byte(mAndsigma))

	h_con = append(h_con, hash.Sum(nil)...)

	message := "\nData just sent by you received at server http://127.0.0.1:8003/upload.\n"

	w.Write([]byte(message))
}

func hashHandler(w http.ResponseWriter, r *http.Request) {
	h := pairing.NewG1().SetFromHash(h_con)
	fmt.Println(h)
    signature := pairing.NewG1().PowZn(h, sk)
	fmt.Println(signature)
	fmt.Fprintf(w, h.String()+"||"+signature.String())
}

func main() {
	instContract, auth, connection := ContractVars()
	ctx := context.Background()

	paramsByte, GetGVal, GetUVal, err := instContract.GetParamsGU(nil)
	functions.CheckError(err)
	
	pairing, err = pbc.NewPairingFromString(string(paramsByte))
	functions.CheckError(err)

	g = pairing.NewG1().SetBytes(GetGVal)
	u = pairing.NewG1().SetBytes(GetUVal)
	sk = pairing.NewZr().Rand()
	pk = pairing.NewG1().PowZn(g, sk)

	functions.FileCrWr("./tmp/private/server/skS", sk.Bytes())

	tx, err := instContract.SetPKS(auth, pk.Bytes())
	functions.CheckError(err)
	//fmt.Println("\nTxn hash is: ", tx.Hash())
	_, err = bind.WaitMined(ctx, connection, tx)
	functions.CheckError(err)
	GetPKSVal, err := instContract.GetPKS(nil)
	functions.CheckError(err)
	fmt.Println("\nGetPKS() Returned: ", GetPKSVal)

	http.HandleFunc("/", sayHello)
	http.HandleFunc("/upload", uploadHandler)
	http.HandleFunc("/hash", hashHandler)
	http.HandleFunc("/query", queryHandler)
	if err := http.ListenAndServe(":8003", nil); err != nil {
		log.Fatal(err)
	}
}

func ContractVars() (*Audit, *bind.TransactOpts, *ethclient.Client) {
	gethPath, nodeKey := functions.GethPathAndKey()
    
    addrByte, err := ioutil.ReadFile("./tmp/contractInfo/contract-address")
	functions.CheckError(err)
    
    connection, err := ethclient.Dial(gethPath)
    functions.CheckError(err)
    fmt.Println("connection Returned: ", connection)
    
    instContract, err := NewAudit(common.HexToAddress(string(addrByte)), connection)
    functions.CheckError(err)
    fmt.Println("instContract Returned: ", instContract)
    
	auth, err := bind.NewTransactor(strings.NewReader(nodeKey), "asdfgh")
	functions.CheckError(err)

	return instContract, auth, connection
}
